import pandas as pd
from datetime import timedelta


def assign_zone(player_x, player_z, zone_coords):
    """
    Based on coordination of the player, finds which zone is the player in
    :param player_x: data.x
    :param player_z: data.z
    :param zone_coords: coordinations of all zones in the map
    :return: the zone number if the x and z are within the map zones and -1 if not
    """
    for zone in zone_coords:
        [zone_number, xtl, xbr, ztl, zbr] = zone
        if (xbr <= player_x <= xtl) and (ztl <= player_z <= zbr):
            return zone_number
    return -1


def zone_visits_revisits(df, zones, experiment):
    """
    Calculate percent zone visited and revisited for one trial stored in df and zones of interest
    :param df: events related to one trial (or events of a specific time period in a trial)
    :param zones: zones of interest
    :param experiment: a postfix that will be added to the variable names
    :return: percent zones visited and revisited in df
    """
    zone_coords = zones[['Zone Number', 'Xcoords-TopLeft', 'XCoords-BotRight',
                         'Zcoords-TopLeft', 'ZCoords-BotRight']].values.tolist()

    df.dropna(subset=['data.x', 'data.z'], inplace=True)

    df['zone'] = df.apply(lambda row: assign_zone(row['data.x'], row['data.z'], zone_coords), axis=1)
    df['zone'] = df['zone'].astype(int)

    df = df.loc[df['zone'] != df['zone'].shift()]

    zone_visits_count = pd.DataFrame(df['zone'].value_counts())
    zone_visits_count = zone_visits_count.drop(-1, errors='ignore')

    zone_visits = zone_visits_count != 0
    zone_revisits = zone_visits_count > 1

    total_zones_count = len(zone_coords)

    percent_zone_visited = '{:.2%}'.format(float(zone_visits.values.sum()) / total_zones_count)
    percent_zone_revisited = '{:.2%}'.format(float(zone_revisits.values.sum()) / total_zones_count)

    visit_revisit_percent_df = pd.DataFrame.from_records([[percent_zone_visited, percent_zone_revisited]],
                                                         columns=["percent_zone_visited_" + experiment,
                                                                  "percent_zone_revisited_" + experiment])

    return visit_revisit_percent_df


def zone_revisits_partially_cleared(triage_events, revisits, victims_zone, start, end, experiment):
    grouped = revisits.groupby('zone')
    count = 0
    for i, zone_revisits in grouped:
        zone_triage_timestamps = triage_events[triage_events['zone'] == i].sort_values(by='msg.timestamp')['msg.timestamp']
        zone_revisit_timestamps = zone_revisits['msg.timestamp']
        num_victims = victims_zone[i] if i in victims_zone.keys() else 0
        if num_victims <= 1:
            continue
        for revisit_timestamp in zone_revisit_timestamps:
            if revisit_timestamp > start and revisit_timestamp < end:
                if len(zone_triage_timestamps) > 0 and revisit_timestamp > zone_triage_timestamps.values[0]:
                    count += 1

    zone_revisit_partially_cleared_df = pd.DataFrame.from_records([[count]], columns=["revisit_partially_cleared" + experiment])
    return zone_revisit_partially_cleared_df


def zone_revisits_fully_cleared(triage_events, revisits, victims_zone, start, end, experiment):
    grouped = revisits.groupby('zone')
    count = 0
    for i, zone_revisits in grouped:
        zone_triage_timestamps = triage_events[triage_events['zone'] == i].sort_values(by='msg.timestamp')['msg.timestamp']
        zone_revisit_timestamps = zone_revisits['msg.timestamp']
        num_victims = victims_zone[i] if i in victims_zone.keys() else 0
        if num_victims < 1 or len(zone_triage_timestamps) < num_victims:
            continue
        for revisit_timestamp in zone_revisit_timestamps:
            if revisit_timestamp > start and revisit_timestamp < end:
                if revisit_timestamp > zone_triage_timestamps.values[-1]:
                    count += 1

    zone_revisit_fully_cleared_df = pd.DataFrame.from_records([[count]], columns=["revisit_fully_cleared" + experiment])
    return zone_revisit_fully_cleared_df


def zone_revisits_not_cleared(triage_events, revisits, victims_zone, start, end, experiment):
    grouped = revisits.groupby('zone')
    count = 0
    for i, zone_revisits in grouped:
        zone_triage_timestamps = triage_events[triage_events['zone'] == i].sort_values(by='msg.timestamp')['msg.timestamp']
        zone_revisit_timestamps = zone_revisits['msg.timestamp']
        num_victims = victims_zone[i] if i in victims_zone.keys() else 0
        if num_victims < 1 or len(zone_triage_timestamps) > 0:
            continue
        for revisit_timestamp in zone_revisit_timestamps:
            if revisit_timestamp > start and revisit_timestamp < end:
                count += 1
    zone_revisit_fully_cleared_df = pd.DataFrame.from_records([[count]], columns=["revisit_not_cleared" + experiment])
    return zone_revisit_fully_cleared_df


def get_triage_events_zone(df, building_zones):
    zone_coords = building_zones[['Zone Number', 'Xcoords-TopLeft', 'XCoords-BotRight',
                                  'Zcoords-TopLeft', 'ZCoords-BotRight']].values.tolist()
    triage_events = df[(df['msg.sub_type'] == 'Event:Triage') & (df['data.triage_state'] == 'SUCCESSFUL')]
    triage_events['zone'] = df.apply(lambda row: assign_zone(row['data.victim_x'], row['data.victim_z'], zone_coords), axis=1)
    return triage_events


def get_revisits_zone(df, building_zones):
    zone_coords = building_zones[['Zone Number', 'Xcoords-TopLeft', 'XCoords-BotRight',
                                  'Zcoords-TopLeft', 'ZCoords-BotRight']].values.tolist()
    df.dropna(subset=['data.x', 'data.z'], inplace=True)
    df['zone'] = df.apply(lambda row: assign_zone(row['data.x'], row['data.z'], zone_coords), axis=1)
    df['zone'] = df['zone'].astype(int)
    df = df[df['zone'] != -1]
    df = df.loc[df['zone'] != df['zone'].shift()]
    grouped = df.groupby('zone')

    output = pd.DataFrame()
    for i, zone_df in grouped:
        zone_revisits = zone_df.sort_values(by='msg.timestamp')[1:]
        output = output.append(zone_revisits)
    return output


def get_victims_per_zone(victims):
    victims_per_zone = victims.groupby('Zone').size().to_dict()
    return victims_per_zone


def building_spatial_variables(df, building):
    """
    Calculated different spatial variables related to trials of one specific building
    :param df: events related to trials
    :param building: an object containing info about the building
    :return: spatial variables
    """

    building_df = df[df['trial_id'].isin(building.trials)]
    building_zones = pd.read_csv(building.zones_file)

    victims = pd.read_csv(building.victims_file)
    victims_per_zone = get_victims_per_zone(victims)

    grouped = building_df.groupby('trial_id')
    dfs = []

    for ti, trial_df in grouped:
        print("Processing Trial : ", ti)
        try:
            trial_df = trial_df[trial_df['data.mission_timer'] != "Mission Timer not initialized."]
            triage_events = get_triage_events_zone(trial_df, building_zones)
            revisits = get_revisits_zone(trial_df, building_zones)

            res_df_simple = zone_visits_revisits(trial_df, building_zones, "simple")
            building_zones['total_victims'] = building_zones['Number of Green'] + building_zones['Number of Yellow']
            building_zones_with_victim = building_zones[building_zones['total_victims'] > 0]
            res_df_with_victim = zone_visits_revisits(trial_df, building_zones_with_victim, "with_victim")

            building_rooms = building_zones[building_zones['Zone Type'] == 3]
            res_df_rooms = zone_visits_revisits(building_df, building_rooms, "rooms")

            building_rooms = building_zones[building_zones['Zone Type'] == 1]
            res_df_hallways = zone_visits_revisits(building_df, building_rooms, "hallways")

            building_rooms = building_zones[building_zones['Zone Type'] == 2]
            res_df_entrances = zone_visits_revisits(building_df, building_rooms, "entrances")

            five_min_threshold = trial_df['msg.timestamp'].min() + timedelta(minutes=5)
            first_5min_df = trial_df[trial_df['msg.timestamp'] < five_min_threshold]
            second_5min_df = trial_df[trial_df['msg.timestamp'] >= five_min_threshold]
            res_df_first_5min = zone_visits_revisits(first_5min_df, building_zones, "first_5min")
            res_df_second_5min = zone_visits_revisits(second_5min_df, building_zones, "second_5min")

            start, end = trial_df['msg.timestamp'].min(), trial_df['msg.timestamp'].max()
            res_df_fully_cleared = zone_revisits_fully_cleared(triage_events, revisits, victims_per_zone, start, end, "")
            res_df_partially_cleared = zone_revisits_partially_cleared(triage_events, revisits, victims_per_zone, start, end, "")
            res_df_fully_cleared_first_5min = zone_revisits_fully_cleared(triage_events, revisits, victims_per_zone, start,
                                                                      five_min_threshold, "_first_5min")
            res_df_partially_cleared_first_5min = zone_revisits_partially_cleared(triage_events, revisits, victims_per_zone, start, five_min_threshold, "_first_5min")
            res_df_fully_cleared_second_5min = zone_revisits_fully_cleared(triage_events, revisits, victims_per_zone,
                                                                       five_min_threshold, end, "_second_5min")
            res_df_partially_cleared_second_5min = zone_revisits_partially_cleared(triage_events, revisits, victims_per_zone, five_min_threshold, end, "_second_5min")
            res_df_not_cleared = zone_revisits_not_cleared(triage_events, revisits, victims_per_zone, start, end, "")
            res_df_not_cleared_first_5min = zone_revisits_not_cleared(triage_events, revisits, victims_per_zone, start,
                                                                      five_min_threshold, "_first_5min")
            res_df_not_cleared_second_5min = zone_revisits_not_cleared(triage_events, revisits, victims_per_zone,
                                                                       five_min_threshold, end, "_second_5min")

            trial_spacial_df = pd.concat([res_df_simple, res_df_with_victim,
                                      res_df_rooms, res_df_hallways, res_df_entrances,
                                      res_df_first_5min, res_df_second_5min,
                                      res_df_fully_cleared, res_df_partially_cleared,
                                      res_df_fully_cleared_first_5min, res_df_partially_cleared_first_5min,
                                      res_df_fully_cleared_second_5min, res_df_partially_cleared_second_5min,
                                      res_df_not_cleared, res_df_not_cleared_first_5min, res_df_not_cleared_second_5min],
                                     axis=1)
            trial_spacial_df['trial_id'] = ti
            dfs.append(trial_spacial_df.set_index('trial_id'))
        except:
            print("Error during processing")
            continue

    if len(dfs) == 0:
        return None
    spacial_variables_df = pd.concat(dfs)
    return spacial_variables_df


