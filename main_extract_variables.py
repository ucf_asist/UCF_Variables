from UCF_Utils import read_and_combine_jsons
import UCF_TemporalVariables
import UCF_PerformanceVariables
import UCF_SpatialVariables
from Building import Building
import pandas as pd
import argparse
import os
import warnings
warnings.filterwarnings("ignore")

variables_file = "./output/Study-1_2020.08/ucf_variables_TrialMessages_FalconEasy.csv"

parser = argparse.ArgumentParser()
parser.add_argument("-d", '--data_directory', action='store', type=str,
                    help='Directory that contains *_messages.json files')
parser.add_argument('data_file', action='store', type=str,
                    help='The CSV file which contains all data from json files')
args = parser.parse_args()

if args.data_directory and not os.path.exists(args.data_file):
    df = read_and_combine_jsons(args.data_directory, args.data_file)
# Loading data #

df = pd.read_csv(args.data_file)

df['msg.timestamp'] = pd.to_datetime(df['msg.timestamp'])

variable_dfs = []

# EXTRACTING UCF TEMPORAL VARIABLES #
triaging_time_df = UCF_TemporalVariables.extract_event_time(df,
                                                            event_type='Event:Triage', state_col='data.triage_state',
                                                            end_states=['SUCCESSFUL', 'UNSUCCESSFUL'])
variable_dfs.append(triaging_time_df)

sprinting_time_df = UCF_TemporalVariables.extract_event_time(df,
                                                             event_type='Event:PlayerSprinting',
                                                             state_col='data.sprinting', end_states=[False])
variable_dfs.append(sprinting_time_df)

navigating_time_df = UCF_TemporalVariables.extract_navigating_time(df)
variable_dfs.append(navigating_time_df)

idle_time_df = UCF_TemporalVariables.extract_idle_time(df)
variable_dfs.append(idle_time_df)

total_time_df = UCF_TemporalVariables.extract_total_time(df)
variable_dfs.append(total_time_df)

# EXTRACTING PERFORMANCE VARIABLES #
saved_victims_df = UCF_PerformanceVariables.extract_saved_victims(df)
variable_dfs.append(saved_victims_df)

map_type = 'easy'

if map_type == 'ucf_test':
    falcon = Building(bname='falcon', zones_file='./building_info/falcon_zoning.csv',
                      victims_file='./building_info/falcon_victims_coords.csv',
                      trials=['1', '4', '7', '9', '11', '12', '15', '28', '29', '30'])
elif map_type == 'easy':
    falcon = Building(bname='falcon', zones_file='./building_info/falcon_zoning_easy.csv',
                  victims_file='./building_info/falcon_victims_coords_easy.csv',
                  trials= ['43', '46', '51', '57', '60', '61', '66', '69', '71', '75', '78',
                          '80', '83' '87', '93', '94',  '99', '100', '108', '109', '116',
                          '120', '123', '128', '130', '133', '137', '141', '143', '145', '153',
                          '155', '157', '165', '171'])
elif map_type == 'medium':
    falcon = Building(bname='falcon', zones_file='./building_info/falcon_zoning_medium.csv',
                  victims_file='./building_info/falcon_victims_coords_medium.csv',
                  trials= ['48', '50', '55', '58', '63', '65', '68', '70', '74', '77', '79',
                           '84', '85', '92', '96', '98', '102', '106', '110', '115', '119',
                           '121', '126',  '127', '131', '135', '136', '139', '144', '146',
                           '151', '156', '158', '163', '170'])
elif map_type == 'hard':
    falcon = Building(bname='falcon', zones_file='./building_info/falcon_zoning_hard.csv',
                  victims_file='./building_info/falcon_victims_coords_hard.csv',
                  trials= ['44', '47', '49', '56', '59', '62', '64', '67', '72', '73', '76',
                           '81', '82', '86', '91', '95', '97', '101', '107', '111', '117',
                           '118', '122', '124', '129', '132', '134', '138', '140', '142', '147',
                           '152', '154', '159', '164', '169'])
elif map_type == 'competency':
    falcon = Building(bname='falcon', zones_file='./building_info/competency_zoning.csv',
                  victims_file='./building_info/competency_victims_coords.csv',
                  trials=['26', '27', '28', '30', '31', '32', '33', '34', '35',
                          '36', '37', '38', '39', '40', '42', '43', '44', '45',
                          '47', '48', '50', '51', '52', '53', '54', '55', '56',
                          '57', '58', '59', '60', '62', '63', '64', '66', '68'])
sparky = Building(bname='sparky', zones_file='./building_info/sparky_zoning.csv',
                  victims_file='./building_info/sparky_victims_coords.csv',
                  trials=['3', '6', '8', '10', '13', '14'])

falcon_spacial_variables_df = UCF_SpatialVariables.building_spatial_variables(df, falcon)
sparky_spacial_variables_df = UCF_SpatialVariables.building_spatial_variables(df, sparky)
spacial_variables_df = pd.concat([falcon_spacial_variables_df, sparky_spacial_variables_df])
variable_dfs.append(spacial_variables_df)

variables = pd.concat(variable_dfs, axis=1)
variables.to_csv(variables_file)
