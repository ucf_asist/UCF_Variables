import pandas as pd


def extract_event_time(df, event_type, state_col, end_states):
    """
    Calculate the time spent performing an event type for each trial
    :param df: combined json files in pandas DataFrame format
    :param event_type: 'msg.sub_type' values in json files (e.g. 'Event:Triage')
    :param state_col: the column that determines the state of the event (e.g. 'data.triage_state')
    :param end_states: there is a record for the event when the event starts and ends. end_states is a list of states
    that represent the ending of an event. (e.g. ['SUCCESSFUL', 'UNSUCCESSFUL'] for triage)
    :return: a pandas DataFrame containing trial id and time spent performing the event in that trial
    """
    event_df = df[df['msg.sub_type'] == event_type]
    grouped = event_df.groupby('trial_id')
    trial_event_times = []
    for ti, grp in grouped:
        grp['time_diff'] = grp['msg.timestamp'].diff()
        trial_event_df = grp[grp[state_col].isin(end_states)]
        event_time = trial_event_df['time_diff'].sum()
        trial_event_times.append([ti, convert(event_time.total_seconds())])
    event_col_name = event_type + "_time"
    event_time_df = pd.DataFrame.from_records(trial_event_times, columns=['trial_id', event_col_name])
    return event_time_df.set_index('trial_id')


def extract_navigating_time(df):
    """
    Calculate navigating time for each trial. Navigating is calculated by observing non-zero movement along x or z axes
    :param df: combined json files in pandas DataFrame format
    :return: a pandas DataFrame containing trial id and time spent navigating in that trial
    """
    nav_df = df[['trial_id', 'msg.timestamp', 'data.mission_timer', 'data.x', 'data.z']].dropna()
    nav_df = nav_df[nav_df['data.mission_timer'] != "Mission Timer not initialized."]
    grouped = nav_df.groupby('trial_id')
    trial_navtime = []
    for ti, grp in grouped:
        grp['xdiff'] = grp['data.x'].diff()
        grp['zdiff'] = grp['data.z'].diff()
        grp['xmove'] = grp['xdiff'] != 0
        grp['zmove'] = grp['zdiff'] != 0
        grp['nav'] = grp['xmove'] & grp['zmove']
        grp.dropna(subset=['msg.timestamp'])
        nav = grp['nav'].tolist()
        t = grp['msg.timestamp'].tolist()
        nav_time = 0
        i = 1
        while i < len(nav):
            if nav[i]:
                start = i - 1
                end = i
                i += 1
                while i < len(nav) and nav[i]:
                    end += 1
                    i += 1
                nav_time += (t[end] - t[start]).total_seconds()
            else:
                i += 1
        trial_navtime.append([ti, convert(nav_time)])
    nav_df = pd.DataFrame.from_records(trial_navtime, columns=['trial_id', 'navigating_time'])
    return nav_df.set_index('trial_id')


def extract_idle_time(df):
    """
    Calculate idle time for each trial. Idle time is calculated by observing zero movement along x or z axes
    :param df: combined json files in pandas DataFrame format
    :return: a pandas DataFrame containing trial id and time spent idle in that trial
    """
    idle_df = df[['trial_id', 'msg.timestamp', 'data.mission_timer', 'data.x', 'data.z']].dropna()
    idle_df = idle_df[idle_df['data.mission_timer'] != "Mission Timer not initialized."]
    
    grouped = idle_df.groupby('trial_id')
    trial_idletime = []
    for ti, grp in grouped:
        group = df.groupby('trial_id').get_group(ti)
        grp['xdiff'] = grp['data.x'].diff()
        grp['zdiff'] = grp['data.z'].diff()
        grp['xmove'] = grp['xdiff'] == 0
        grp['zmove'] = grp['zdiff'] == 0
        grp['idle'] = grp['xmove'] & grp['zmove']
        grp.dropna(subset=['msg.timestamp'])
        idle = grp['idle'].tolist()
        t = grp['msg.timestamp'].tolist()
        idle_time = 0
        i = 1
        while i < len(idle):
            if idle[i]:
                start = i - 1
                end = i
                i += 1
                while i < len(idle) and idle[i]:
                    end += 1
                    i += 1
                idle_time += (t[end] - t[start]).total_seconds()
            else:
                i += 1
        trial_idletime.append([ti, convert(idle_time)])
    idle_df = pd.DataFrame.from_records(trial_idletime, columns=['trial_id', 'idle_time'])
    return idle_df.set_index('trial_id')


def extract_total_time(df):
    """
    Calculate total time for each trial. Total is the difference in time between the first timestamp and 
    the final timestamp of the message from a Trial.
    :param df: combined json files in pandas DataFrame format
    :return: a pandas DataFrame containing trial id and total time spent in that trial
    """
    time_df = df[['trial_id', 'msg.timestamp', 'data.mission_timer']].dropna()
    time_df = time_df[time_df['data.mission_timer'] != "Mission Timer not initialized."]
    
    grouped = time_df.groupby('trial_id')
    trial_totaltime = []
    for ti, grp in grouped:
        grp.dropna(subset=['msg.timestamp'])
        t = grp['msg.timestamp'].tolist()
        total_time = convert((max(t) - min(t)).total_seconds())
        trial_totaltime.append([ti, total_time])
    nav_df = pd.DataFrame.from_records(trial_totaltime, columns=['trial_id', 'total_time'])
    return nav_df.set_index('trial_id')


def convert(seconds): 
    seconds = seconds % (24 * 3600) 
    hour = seconds // 3600
    seconds %= 3600
    minutes = seconds // 60
    seconds %= 60
      
    return "%d:%02d:%02d" % (hour, minutes, seconds)